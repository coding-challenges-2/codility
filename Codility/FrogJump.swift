//
//  FrogJump.swift
//  Codility
//
//  Created by Imthathullah M on 07/03/20.
//  Copyright © 2020 Imthathullah M. All rights reserved.
//

import Foundation

class FrogJump {
    
    private init() { }
    
    static let shared = FrogJump()
    
    func test() {
        print(solution(10, 85, 30))
    }
    
    public func solution(_ X : Int, _ Y : Int, _ D : Int) -> Int {
        
        guard X < Y else { return 0 }
        
        let distance = Y - X
        let quotient = distance / D
        let reminder = distance % D
        
        if reminder > 0 {
            return quotient + 1
        }
        
        return quotient
    }

}

class MissingElement {
    
    private init() { }
    
    static let shared = MissingElement()
    
    func test() {
        var a = [1,2,8,3,6,7,4,5]
        var b: [Int] = []
        print(solution(&a))
        print(solution(&b))
    }

    public func solution(_ A : inout [Int]) -> Int {
        // write your code in Swift 4.2.1 (Linux)
        A.sort()
        guard let startValue = A.first else { return 0 }
        
        for (index, value) in A.enumerated() {
            if value - startValue != index {
                return startValue + index
            }
        }
        
        return (A.last ?? 0) + 1
    }
    
}

class TapeEquilibrium {
    
    private init() { }
    
    static let shared = TapeEquilibrium()
    
    func test() {
        var a = [1,2,8,3,6,7,4,5]
        print(solution(&a))
        
        var b = [3,1,2,4,3]
        print(solution(&b))
    }
    
    public func solution(_ A : inout [Int]) -> Int {
        guard A.count > 1,
            A.count < 100001 else {
            return 0
        }
        
        let forwardSum = A.successiveSum
        
        guard let totalSum = forwardSum.last else {
            return 0
        }
        
        var reverseSum = forwardSum
        
        for index in reverseSum.indices {
            if index == 0 {
                reverseSum[0] = totalSum
            } else {
                reverseSum[index] = totalSum - forwardSum[index - 1]
            }
        }
        
        var minDiffernce = 2000
        
        for (index, value) in reverseSum.enumerated() {
            if index == 0 { continue }
            
            let difference = abs(value - forwardSum[index - 1])
            
            if difference < minDiffernce {
                minDiffernce = difference
            }
        }
        
        return minDiffernce
    }
    
    
}

extension Array where Element: AdditiveArithmetic {
    
    var successiveSum: [Element] {
        
        var result = [Element]()
        
        for (index, number) in enumerated() {
            if index == 0 {
                result.append(number)
            } else {
                result.append(result[index-1] + number)
            }
        }
        
        return result
    }
}
