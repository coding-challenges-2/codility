//
//  BannerCover.swift
//  Codility
//
//  Created by Imthathullah M on 07/03/20.
//  Copyright © 2020 Imthathullah M. All rights reserved.
//

import Foundation

class BannerCover {
    
    private init() { }
    
    static let shared = BannerCover()
    
    func test() {
        
//        var a = [1, 1, 7, 6, 6, 6]
//        print(solution(&a))
//
//        var b = [3,1,2,4,3]
//        print(solution(&b))
//
        // working
//        var c = [7,7,3,7,7]
//        print(solution(&c))
        
        // working
        var d = [4,7,3,7,2]
        print(solution(&d))
        
        // working
//        var e = [5, 3, 5, 2, 1]
//        print(solution(&e))
        
        // working
        var f = [2,1,5,3,5]
        print(solution(&f))
        
        
    }
    
    public func solution(_ H : inout [Int]) -> Int {
        
        
        func getLeadingArea(for maxHeight: Int, in index: Int) -> Int {
            var secondHigh = 0
            for i in 0..<index {
                if H[i] > secondHigh {
                    secondHigh = H[i]
                }
            }
            let beforeArea = secondHigh * index
            let afterArea = maxHeight * (H.count - index)
            return beforeArea + afterArea
        }
        
        func getTrailingArea(for maxHeight: Int, in index: Int) -> Int {
            var secondHigh = 0
            for i in index + 1 ..< H.count {
                if H[i] > secondHigh {
                    secondHigh = H[i]
                }
            }
            let beforeArea = maxHeight * (index + 1)
            let afterArea = secondHigh * (H.count - index - 1)
            return beforeArea + afterArea
        }
        
        guard !H.isEmpty else {
            return .zero
        }
        
        let sortedValues = H.sortedValuesWithIndices
        let lastIndex = H.count - 1
        
        guard let maxHeight = sortedValues.first,
            maxHeight.key > .zero else {
                return .zero
        }
        
        if maxHeight.value.count == 1 {
            
        }
        
        // max height is present more than once
        if let lastMaxHeightIndex = maxHeight.value.last,
            let firstMaxHeightIndex = maxHeight.value.first {
            
            if lastMaxHeightIndex == lastIndex, firstMaxHeightIndex == .zero {
                return maxHeight.key * H.count
            }
            
            if lastMaxHeightIndex != lastIndex, firstMaxHeightIndex != .zero {
                // calcuate min area based on firstMaxHeightIndex
//                var secondHigh = 0
//                for index in 0..<firstMaxHeightIndex {
//                    if H[index] > secondHigh {
//                        secondHigh = H[index]
//                    }
//                }
//                var beforeArea = secondHigh * firstMaxHeightIndex
//                var afterArea = maxHeight.key * (H.count - firstMaxHeightIndex)
                let leadingArea = getLeadingArea(for: maxHeight.key, in: firstMaxHeightIndex)
                
//                secondHigh = 0
//                for index in lastMaxHeightIndex + 1 ..< H.count {
//                    if H[index] > secondHigh {
//                        secondHigh = H[index]
//                    }
//                }
//                beforeArea = maxHeight.key * (lastMaxHeightIndex + 1)
//                afterArea = secondHigh * (H.count - lastMaxHeightIndex - 1)
                let trailingArea = getTrailingArea(for: maxHeight.key, in: lastMaxHeightIndex)
                
                if leadingArea < trailingArea { return leadingArea }
                
                return trailingArea
            }
            
            if firstMaxHeightIndex == .zero {
                var secondHigh = 0
                for index in lastMaxHeightIndex + 1 ..< H.count {
                    if H[index] > secondHigh {
                        secondHigh = H[index]
                    }
                }
                let beforeArea = maxHeight.key * (lastMaxHeightIndex + 1)
                let afterArea = secondHigh * (H.count - lastMaxHeightIndex - 1)
                return beforeArea + afterArea
            }
            
            if lastMaxHeightIndex == lastIndex {
                return getLeadingArea(for: maxHeight.key, in: firstMaxHeightIndex)
//                var secondHigh = 0
//                for index in 0..<firstMaxHeightIndex {
//                    if H[index] > secondHigh {
//                        secondHigh = H[index]
//                    }
//                }
//                let beforeArea = secondHigh * firstMaxHeightIndex
//                let afterArea = maxHeight.key * (H.count - firstMaxHeightIndex)
//                return beforeArea + afterArea
            }
        }
        
        return .zero
    }
}

extension Array where Element == Int {
    
    fileprivate var sortedValuesWithIndices: [(key: Int, value: [Int])] {
        var dict = [Int: [Int]]()
        
        for (index, height) in enumerated() {
            if dict[height] != nil {
                dict[height]?.append(index)
            } else {
                dict[height] = [index]
            }
        }
        
        
        return dict.sorted(by: { $0.key > $1.key })
    }
}

